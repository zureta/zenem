<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <title> - ZENEM - </title>
        <meta http-equiv="Content-Type" content="text/html;"/>
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/style.css" />
        <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'/>
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        <!-- <script type="text/javascript" src="js/Melbourne.font.js"></script> -->
    </head>

    <body>
                               <!-- MENU -->

        <div id="headercont" class="clearfix">
            <div id="header">
                <div id="logocont">
                    <a href="index.html">
                        <img class="logo" src="imagens/pngwing.com.png" alt="Logo ZENEM.">
                    </a>
                </div>
                <div id="menucont">
                    <ul>
                        <li><a title="" href="">Home</a></li>
                        <li><a title="" href="">ZENEM</a></li>
                        <li><a title="" href="">Meditação</a></li>
                        <li><a title="" href="">Contato</a></li>
                    </ul>
                </div>
            </div>
        </div>
                           <!-- MENU FINAL -->
     
        <div id="maincont" class="clearfix">
            <div id="main" class="clearfix">
                <div id="mainleft">
            
                    <h1>Não deixe a <span>Ansiedade</span> te confundir no ENEM.</h1>

                    <img class="zen" alt="Pessoa meditando." src="imagens/zen.svg"/>

                    <p>Você vai fazer o <a title="site ENEM" href="https://enem.inep.gov.br">ENEM</a> este ano? É mais fácil ter sucesso quando você se sente bem, menos ansioso e menos estressado. Quer eles admitam ou não, todo mundo se sente estressado e / ou ansioso em algum momento durante as provas. Por isso, neste artigo vamos ensinar a como evitar a ansiedade no Enem.</p>

                    <ol>
                        <li><strong>Coma adequadamente</strong></li>
                        <p>Seu corpo precisa dos nutrientes que recebe dos alimentos para continuar funcionando adequadamente. A comida que você come afeta como você se sente tanto emocional quanto fisicamente. Por exemplo, alimentos com muitas gorduras ou açúcares podem fazer você se sentir pesado ou lento. Quando nosso corpo tem o combustível e os nutrientes de que precisa, fica muito mais fácil gerenciar sentimentos de estresse e ansiedade.</p>

                        <li><strong>Durma  bem </strong></li>
                        <p>Relaxe antes de dormir. Sua cama é um santuário, não uma mesa. Um bom sono ajuda a lembrar o que aprendeu. Durma o suficiente, principalmente nos dias anteriores aos seus exames.O sono é um elemento fundamental no processo de aprendizado ele é responsável por garantir um bom funcionamento da sua memória, melhorar seu humor e, inclusive, é capaz de prevenir doenças.</p>

                        <li><strong>Faça exercícios</strong></li>
                        <p>Atividade física, como correr e nadar, fará você se sentir calmo, fresco e energético por horas. Portanto, crie exercícios em seu horário.</p>

                        <li><strong>Estratégias de distração</strong></li>
                        <p>Use algumas estratégias de distração que podem ajudar a gerenciar seus sentimentos estressantes ou ansiosos, como usar uma bola de estresse, mascar chiclete, beber um gole de água gelada, usar brinquedos de agitação, como um elástico no pulso ou massas de modelar como Play-Doh.</p>

                        <li><strong>Pensamentos positivos</strong></li>
                        <p>Combate os pensamentos de preocupação ou negativos, como “vou falhar” ou “não posso fazer isso” com pensamentos positivos ou declarações de torcida, como “eu entendi” ou “vou tentar o meu melhor, eu sei o meu coisa”. Escreva estas declarações positivas ou animadoras de torcida e publique-as na sua área de estudo.</p>

                        <li><strong>Técnicas de relaxamento</strong></li>
                        <p>Reduza os sentimentos de estresse ou ansiedade ao estudar ou escrever exames usando exercícios respiratórios. Por exemplo: reserve um minuto para fechar os olhos, inspire até três e depois expire até cinco e depois repita. Leva apenas um momento e ajuda o corpo e a mente a relaxar, assim você estará em um estado de espírito melhor para se concentrar.</p>

                        <li><strong>Converse com alguém</strong></li>
                        <p>Se você ainda se sentir estressado demais, converse com alguém em quem confie; se é um pai, professor, conselheiro ou amigo que também tenha que lidar com a ansiedade no Enem. Às vezes, apenas falar sobre coisas pode fazer você se sentir melhor e a pessoa com quem você conversa pode ajudá-lo a colocar as coisas em perspectiva. Por fim, não perca de vista que, embora as coisas pareçam intensas agora, não vão durar para sempre. Encontrar saídas e estratégias saudáveis ​​e positivas para lidar com sentimentos de estresse e ansiedade no exame pode ajudar você a se sentir mais no controle.</p>

                        <li><strong>Reduza a ansiedade no Enem com um estudo eficaz</strong></li>
                        <p><strong>Lembre-se que você já aprendeu o conteúdo:</strong> a maneira mais efetiva de estudar é ir nas aulas, fazer boas anotações, fazer exercícios, ou seja, ser estudar de maneira ativa. É extremamente importante que você aprenda a montar seu próprio horário de estudo e aprender o que funciona melhor (ou pior) pra você. As pessoas são diferentes e o que funciona pra uma não necessariamente vai funcionar pra outra, nunca esqueça disso.</p>

                        <li><strong>Não deixe para estudar na véspera da prova</strong></li>
                        <p>Você já ouviu falar do famoso método de estudo Jack Bauer, ou seja, aquele que tenta fazer tudo em 24 horas? 🙂 Brincadeiras a parte, todos sabemos que costumamos deixar tudo para última hora, inclusive os estudos. Tem gente que resolve estudar em outubro e busca “milagres” para ser aprovado. Você precisa ser consistente nos estudos. Uma das coisas mais importantes para serem realizadas durante o ano é criar uma estratégia de revisões periódicas. Não tem jeito, se você estuda um conteúdo em janeiro e não fizer revisões dele de tempos em tempos, dificilmente você estará apto(a) a resolver questões daquele assunto na prova. Planejamento é essencial no caminho mas não caia na tentação de passar mais tempo organizando e planejando do que, de fato, estudando. Então muita atenção nisso, ok?</p>

                        <li><strong>Seja organizado</strong></li>
                        <p>Pense na quantidade de informação que você recebe em um dia na escola / cursinho. Você precisa de uma maneira lógica de armazená-las (fisica e mentalmente). Mapas mentais ajudam muito nesse processo de organização. Veja o vídeo abaixo do canal <strong>“seja um estudante melhor.“</strong></p>

                    </ol>

                    <h2>Exemplos que ajudam a lidar com a ansiedade no Enem.</h2>
                    <blockquote>Você também pode ler nossa seção de depoimentos <a href="ZENEM.html"><i>"ZENEM"</i></a>, com casos reais de muitas pessoas que conseguiram superar esse momento tão difícil e encontraram maneiras de lidar com a pressão do vestibular e com a ansiedade no Enem.

                       <br> Conte sempre com a gente.</br>
                     </blockquote>
                </div>
                <div id="mainright">
                    <h3>REGISTRE SUAS <br> <span>ATIVIDADES</span></h3> 
                    <p>Caso queira, registre suas impressões por escrito depois de cada meditação. Você pode repetir o exercício abaixo quantas vezes sentir necessidade.</p>
                    <p> A partir da prática contínua, será possível notar que suas percepções podem mudar a cada vez que realizar o exercício. De tempos em tempos, releia suas anotações. Afinal, novas percepções sempre poderão surgir.</p>
                </div>
            </div>
        </div>

        <div id="footercont">
            <div id="footer">
                <p>Website Design by  <a title="derby web design" href="https://www.facebook.com/jano.clavicarius" rel="external">Pyero Ayres</a></p>
            </div>
        </div>
        
    </body>
</html>